# Project-Planning-System

username: telerikproject42@gmail.com
password: telerikproject42

## Prerequisites

The technologies below must be installed in order to run this application:

#### Front End:

* Typescript
* Angular 9+
* Angular CLI

#### Back End:

* Firebase
