import { SharedModule } from '../../src/app/shared/shared.module';
import { UsersService } from '../../src/app/users/users.service';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireFunctions, AngularFireFunctionsModule } from '@angular/fire/functions';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { async, TestBed } from '@angular/core/testing';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { AngularFireModule } from '@angular/fire';
import { callbackify } from 'util';

describe('UsersService', () => {

    let service: UsersService;

    let angularFirestore;
    let angularFireAuth;
    let angularFireFunctions;
    let firebaseImport;

    let collection;

    beforeEach(async(() => {

        jest.clearAllMocks();

        collection = {
            valueChanges() { /* empty */ }
        };

        angularFirestore = {
            collection() { return collection; },
        };

        angularFireAuth = {
            sendPasswordResetEmail() { /* empty */ }
        };

        angularFireFunctions = {
            httpsCallable() { /* empty */ }
        };

        firebaseImport = {
            firestore() { /* empty */ }
        };

        TestBed.configureTestingModule({
            imports: [
                SharedModule,
                AngularFireModule,
                AngularFireFunctionsModule,
                AngularFireAuthModule,
                AngularFirestoreModule,
                AngularFireStorageModule,
            ],
            providers: [
                UsersService
            ]
        })
            .overrideProvider(AngularFirestore, { useValue: angularFirestore })
            .overrideProvider(AngularFireAuth, { useValue: angularFireAuth })
            .overrideProvider(AngularFireFunctions, { useValue: angularFireFunctions })
            .overrideProvider(firebase, { useValue: firebaseImport });

        service = TestBed.get(UsersService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getAllUsers should', () => {

        it('call the angularFirestore.collection() once', () => {

            // Arrange
            const collectionUsers = 'Users';

            const spy = jest.spyOn(angularFirestore, 'collection').mockReturnValue(collection);

            // Act
            service.getAllUsers();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call the angularFirestore.collection() with correct parameter', () => {

            // Arrange
            const collectionUsers = 'Users';

            const spy = jest.spyOn(angularFirestore, 'collection').mockReturnValue(collection);

            // Act
            service.getAllUsers();

            // Assert
            expect(spy).toHaveBeenCalledWith(collectionUsers);
        });

        it('call the angularFirestore.collection().valueChanges() once', () => {

            // Arrange
            const collectionUsers = 'Users';

            jest.spyOn(angularFirestore, 'collection').mockReturnValue(collection);
            const spy = jest.spyOn(collection, 'valueChanges');

            // Act
            service.getAllUsers();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('getAllManagers should', () => {

        it('call the angularFirestore.collection() once', () => {

            // Arrange
            const collectionUsers = 'Users';

            const spy = jest.spyOn(angularFirestore, 'collection').mockReturnValue(collection);

            // Act
            service.getAllManagers();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call the angularFirestore.collection().valueChanges() once', () => {

            // Arrange
            const collectionUsers = 'Users';

            jest.spyOn(angularFirestore, 'collection').mockReturnValue(collection);
            const spy = jest.spyOn(collection, 'valueChanges');

            // Act
            service.getAllUsers();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('getIndividualUser should', () => {

        it('call the angularFirestore.collection() once', () => {

            // Arrange
            const collectionUsers = 'Users';

            const doc = {
                valueChanges() { /* empty */ }
            };

            const spy = jest.spyOn(angularFirestore, 'collection').mockReturnValue(doc);

            // Act
            service.getAllManagers();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        // it('call the angularFirestore.collection() with correct parameter', () => {

        //     // Arrange
        //     const collectionUsers = 'Users';

        //     const doc = {
        //         valueChanges() { /* empty */ }
        //     };

        //     const spy = jest.spyOn(angularFirestore, 'collection').mockReturnValue(doc);

        //     // Act
        //     service.getAllManagers();

        //     // Assert
        //     expect(spy).toHaveBeenCalledWith(collectionUsers);
        // });

        // it('call the angularFirestore.collection().doc() once', () => {

        //     // Arrange
        //     const collectionUsers = 'Users';

        //     const valueChanges = () => {};

        //     const doc = {
        //         valueChanges
        //     };

        //     jest.spyOn(angularFirestore, 'collection').mockReturnValue(doc);
        //     const spy = jest.spyOn(collection, 'doc').mockReturnValue(valueChanges);

        //     // Act
        //     service.getAllManagers();

        //     // Assert
        //     expect(spy).toHaveBeenCalledTimes(1);
        // });
    });
});
