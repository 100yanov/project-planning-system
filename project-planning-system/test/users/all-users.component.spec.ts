import { IManagerEmployee } from '../../src/app/common/interfaces/manager-employee';
import { ISubmitUser } from './../../src/app/common/interfaces/submit-user';
import { IUser } from './../../src/app/common/interfaces/user';
import { UserProfileComponent } from './../../src/app/users/user-profile/user-profile.component';
import { UsersRoutingModule } from './../../src/app/users/users-routing.module';
import { SharedModule } from './../../src/app/shared/shared.module';
import { UsersModalComponent } from './../../src/app/users/users-modal/users-modal.component';
import { AllUsersComponent } from './../../src/app/users/all-users/all-users.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { UsersService } from './../../src/app/users/users.service';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from './../../src/app/auth/auth.service';
import { Router } from '@angular/router';
import { NotificationService } from './../../src/app/core/services/notification.service';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Observable } from 'rxjs';
import { EventEmitter } from '@angular/core';

describe('AllUsersComponent', () => {

    let usersService;
    let authService;
    let dialog;
    let router;
    let notificationService;

    let fixture: ComponentFixture<AllUsersComponent>;
    let component: AllUsersComponent;

    beforeEach(async(() => {

        jest.clearAllMocks();

        usersService = {
            getAllUsers() { /* empty */ },
            createAdminOrManager() { /* empty */ },
            sendPasswordResetEmail() { /* empty */ },
            createEmployee() { /* empty */ },
            getIndividualUserDataPromise() { /* empty */ },
            updateEmployee() { /* empty */ }
        };

        authService = {
            get getLoggedUser() {
                return of();
            }
        };

        dialog = {
            open() { /* empty */ }
        };

        router = {
            navigate() { /* empty */ }
        };

        notificationService = {
            error() { /* empty */ },
            success() { /* empty */ }
        };

        TestBed.configureTestingModule({

            declarations: [
                AllUsersComponent,
                UserProfileComponent,
                UsersModalComponent
            ],
            imports: [
                SharedModule,
                UsersRoutingModule,
                RouterTestingModule
            ],
            providers: [
                UsersService, AuthService, MatDialog, NotificationService
            ]
        })
            .overrideProvider(UsersService, { useValue: usersService })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(MatDialog, { useValue: dialog })
            .overrideProvider(Router, { useValue: router })
            .overrideProvider(NotificationService, { useValue: notificationService })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(AllUsersComponent);
                component = fixture.componentInstance;
            });
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit() should', () => {

        it('call usersService.getAllUsers() once', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            const spy = jest.spyOn(usersService, 'getAllUsers').mockReturnValue(of(mockAllUsers));

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('subscribe to usersService.getAllUsers() observable and save the emitted value', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            const spy = jest.spyOn(usersService, 'getAllUsers').mockReturnValue(of(mockAllUsers));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.users).toEqual(mockAllUsers);
        });

        it('call authService.getLoggedUser() once', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            jest.spyOn(usersService, 'getAllUsers').mockReturnValue(of(mockAllUsers));
            const spy = jest.spyOn(authService, 'getLoggedUser', 'get').mockReturnValue(of(mockUser));

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('subscribe to authService.getLoggedUser() observable and save the emitted value', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            jest.spyOn(usersService, 'getAllUsers').mockReturnValue(of(mockAllUsers));
            const spy = jest.spyOn(authService, 'getLoggedUser', 'get').mockReturnValue(of(mockUser));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.loggedUser).toEqual(mockUser);
        });

        it('set isLoggedUserAdmin to true, when userSkills includes Admin', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Admin'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            jest.spyOn(usersService, 'getAllUsers').mockReturnValue(of(mockAllUsers));
            const spy = jest.spyOn(authService, 'getLoggedUser', 'get').mockReturnValue(of(mockUser));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.isLoggedUserAdmin).toBeTruthy();
        });

        it('set isLoggedUserAdmin to true, when userSkills includes Admin', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            jest.spyOn(usersService, 'getAllUsers').mockReturnValue(of(mockAllUsers));
            const spy = jest.spyOn(authService, 'getLoggedUser', 'get').mockReturnValue(of(mockUser));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.isLoggedUserAdmin).toBeFalsy();
        });
    });

    describe('openAddNewEmployeeDialog should', () => {

        it('call dialog.open() once', () => {

            // Arrange
            const spy = jest.spyOn(dialog, 'open').mockReturnValue({
                componentInstance: {
                    userSubmited: new Observable()
                }
            });

            // Act
            component.openAddNewEmployeeDialog();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call notificationService.error() once, when emitted user already exists', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user1',
                lastName: 'user1',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: null,
                userSkills: [],
            };

            component.users = mockAllUsers;

            const userModalComponent = {
                componentInstance: {
                    userSubmited: new EventEmitter<ISubmitUser>()
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            const spy = jest.spyOn(notificationService, 'error');

            // Act
            component.openAddNewEmployeeDialog();
            userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call notificationService.error(), with correct message', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user1',
                lastName: 'user1',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: null,
                userSkills: [],
            };

            component.users = mockAllUsers;

            const userModalComponent = {
                componentInstance: {
                    userSubmited: new EventEmitter<ISubmitUser>()
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            const spy = jest.spyOn(notificationService, 'error');

            // Act
            component.openAddNewEmployeeDialog();
            userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith('Employee with such email already exist!');
        });

        it('not call notificationService.error(), when emitted user does not exists', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockAllUsers: IUser[] = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user1',
                lastName: 'user1',
                email: 'user1@email.com',
                position: 'Junior Developer',
                manager: null,
                userSkills: [],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            component.users = mockAllUsers;

            const userModalComponent = {
                componentInstance: {
                    userSubmited: new EventEmitter<ISubmitUser>()
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            const spy = jest.spyOn(notificationService, 'error');

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());


            // Act
            component.openAddNewEmployeeDialog();
            userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(0);
        });

        it('call usersService.createAdminOrManager() once, when userSkills includes Management', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: null,
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: new EventEmitter<ISubmitUser>()
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            const spy = jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());


            // Act
            component.openAddNewEmployeeDialog();
            userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call usersService.createAdminOrManager() with correct parameter, when userSkills includes Management', () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: null,
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: new EventEmitter<ISubmitUser>()
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            const spy = jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());


            // Act
            component.openAddNewEmployeeDialog();
            userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith({ ...mockUser, ...mockSubmitUser });
        });

        it('call usersService.sendPasswordResetEmail() once, when userSkills includes Management', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: undefined
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            const spy = jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call usersService.sendPasswordResetEmail() with correct parameter, when userSkills includes Management', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: undefined
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            const spy = jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith('user@email.com');
        });

        it('call usersService.createEmployee() once', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            const spy = jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call usersService.createEmployee() with correct parameter', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            const spy = jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith(mockNewUser);
        });

        it('call usersService.getIndividualUserDataPromise() once', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            const spy = jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call usersService.getIndividualUserDataPromise() with correct parameter', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            const spy = jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith(mockNewUser.userId);
        });

        it('call usersService.updateEmployee() once', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;
            component.users = [mockUser];

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            const spy = jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call usersService.updateEmployee() with correct parameter', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string1',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const mockManagerEmployee: IManagerEmployee = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                availableTime: 8,
            };

            const mockNewEmployeeManager: IUser = {
                userId: 'string1',
                firstName: 'user1',
                lastName: 'user1',
                email: 'user1@email.com',
                userSkills: ['Management'],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [mockManagerEmployee],
                manager: null
            };

            component.users = [mockNewEmployeeManager];

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            const spy = jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith(mockNewEmployeeManager);
        });

        it('call notificationService.success() once, when user is created', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string1',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const mockManagerEmployee: IManagerEmployee = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                availableTime: 8,
            };

            const mockNewEmployeeManager: IUser = {
                userId: 'string1',
                firstName: 'user1',
                lastName: 'user1',
                email: 'user1@email.com',
                userSkills: ['Management'],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [mockManagerEmployee],
                manager: null
            };

            component.users = [mockNewEmployeeManager];

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            const spy = jest.spyOn(notificationService, 'success');

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('call notificationService.success() with correct message, when user is created', async () => {

            // Arrange
            const mockUser: IUser = {
                userId: 'string',
                firstName: '',
                lastName: '',
                email: '',
                userSkills: [],
                position: '',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            const mockSubmitUser: ISubmitUser = {
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                manager: {
                    userId: 'string1',
                    firstName: 'user1',
                    lastName: 'user1'
                },
                userSkills: ['Management'],
            };

            const mockNewUser: IUser = { ...mockUser, ...mockSubmitUser };

            const mockManagerEmployee: IManagerEmployee = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                position: 'Junior Developer',
                availableTime: 8,
            };

            const mockNewEmployeeManager: IUser = {
                userId: 'string1',
                firstName: 'user1',
                lastName: 'user1',
                email: 'user1@email.com',
                userSkills: ['Management'],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [mockManagerEmployee],
                manager: null
            };

            component.users = [mockNewEmployeeManager];

            const userModalComponent = {
                componentInstance: {
                    userSubmited: {
                        subscribtions: [],
                        subscribe(next) {
                            this.subscribtions.push(next);
                        },
                        async emit(...args) {
                            for (const next of this.subscribtions) {
                                await next(...args);
                            }
                        }
                    }
                }
            };

            jest.spyOn(dialog, 'open').mockReturnValue(userModalComponent);

            jest.spyOn(usersService, 'createAdminOrManager').mockImplementation(() => Promise.resolve());
            jest.spyOn(usersService, 'sendPasswordResetEmail').mockImplementation(() => Promise.resolve());

            jest.spyOn(usersService, 'createEmployee').mockImplementation(() => Promise.resolve(mockNewUser.userId));
            jest.spyOn(usersService, 'getIndividualUserDataPromise').mockImplementation(() => Promise.resolve(mockNewUser));
            jest.spyOn(usersService, 'updateEmployee').mockImplementation(() => Promise.resolve());

            const spy = jest.spyOn(notificationService, 'success');

            // Act
            component.openAddNewEmployeeDialog();
            await userModalComponent.componentInstance.userSubmited.emit(mockSubmitUser);

            // Assert
            expect(spy).toHaveBeenCalledWith('Successfully created employee');
        });
    });

    describe('filterDirectSubordinates should', () => {

        it('filter logged user direct subordinates, when event', () => {

            // Arrange
            const event = new MouseEvent('click');
            spyOn(event, 'preventDefault');

            const mockLoggedUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Management'],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            const mockUser: IUser = {
                userId: 'string1',
                firstName: 'user1',
                lastName: 'user1',
                email: 'user1@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: {
                    userId: 'string',
                    firstName: 'user',
                    lastName: 'user',
                }
            };

            component.loggedUser = mockLoggedUser;
            component.users = [mockLoggedUser, mockUser];

            // Act
            component.filterDirectSubordinates(event);

            // Assert
            expect(component.usersToShow.data).toEqual([mockUser]);
        });
    });

    describe('filterByField should', () => {

        it('filter by email', () => {

            // Arrange
            const field = 'email';
            const filter = 'user';

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.users = [mockUser];

            // Act
            component.filterByField(field, filter);

            // Assert
            expect(component.usersToShow.data).toEqual([mockUser]);
        });

        it('filter by firstName and lastName', () => {

            // Arrange
            const field = 'firstName || lastName';
            const filter = 'user';

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.users = [mockUser];

            // Act
            component.filterByField(field, filter);

            // Assert
            expect(component.usersToShow.data).toEqual([mockUser]);
        });

        it('set empty array, when filter has no match', () => {

            // Arrange
            const field = 'firstName || lastName';
            const filter = 'mock';

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.users = [mockUser];

            // Act
            component.filterByField(field, filter);

            // Assert
            expect(component.usersToShow.data).toEqual([]);
        });
    });

    describe('filterByManager should', () => {

        it('filter users, when user has manager', () => {

            // Arrange
            const filter = 'user';
            const defaultValue = 'Self-managed';

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: {
                    userId: 'string1',
                    firstName: 'user1',
                    lastName: 'user1',
                }
            };

            component.users = [mockUser];

            // Act
            component.filterByManager(filter, defaultValue);

            // Assert
            expect(component.usersToShow.data).toEqual([mockUser]);
        });

        it('filter users, when user has no manager', () => {

            // Arrange
            const filter = 'self';
            const defaultValue = 'Self-managed';

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.users = [mockUser];

            // Act
            component.filterByManager(filter, defaultValue);

            // Assert
            expect(component.usersToShow.data).toEqual([mockUser]);
        });

        it('set empty array, when filter has no match', () => {

            // Arrange
            const filter = 'user';
            const defaultValue = 'Self-managed';

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Front-end Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.users = [mockUser];

            // Act
            component.filterByManager(filter, defaultValue);

            // Assert
            expect(component.usersToShow.data).toEqual([]);
        });
    });
});
