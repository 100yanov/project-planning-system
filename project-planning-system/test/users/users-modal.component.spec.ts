import { ISkill } from './../../src/app/common/interfaces/skill';
import { IUser } from './../../src/app/common/interfaces/user';
import { IBasicUser } from '../../src/app/common/interfaces/basic-user';
import { SkillsService } from './../../src/app/skills/skills.service';
import { UsersRoutingModule } from './../../src/app/users/users-routing.module';
import { SharedModule } from './../../src/app/shared/shared.module';
import { UserProfileComponent } from './../../src/app/users/user-profile/user-profile.component';
import { AllUsersComponent } from './../../src/app/users/all-users/all-users.component';
import { UsersModalComponent } from './../../src/app/users/users-modal/users-modal.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { UsersService } from './../../src/app/users/users.service';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

describe('UsersModalComponent', () => {

    let usersService;
    let skillsService;
    let formBuilder;

    let fixture: ComponentFixture<UsersModalComponent>;
    let component: UsersModalComponent;

    beforeEach(async(() => {

        jest.clearAllMocks();

        usersService = {
            getAllManagers() { /* empty */ }
        };

        skillsService = {
            getAllEmployeeSkills() { /* empty */ }
        };

        formBuilder = {
            group() { /* empty */ }
        };

        TestBed.configureTestingModule({

            declarations: [
                AllUsersComponent,
                UserProfileComponent,
                UsersModalComponent
            ],
            imports: [
                SharedModule,
                UsersRoutingModule
            ],
            providers: [
                UsersService,
                SkillsService,
                FormBuilder
            ]
        })
            .overrideProvider(UsersService, { useValue: usersService })
            .overrideProvider(SkillsService, { useValue: skillsService })
            .overrideProvider(FormBuilder, { useValue: formBuilder })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(UsersModalComponent);
                component = fixture.componentInstance;
            });
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit() should', () => {

        it('call usersService.getAllManagers() once', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockAllSkills: ISkill[] = [mockSkill];

            const spy = jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('subscribe to usersService.getAllManagers() observable and save the emitted value', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockAllSkills: ISkill[] = [mockSkill];

            const spy = jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.allManagers).toEqual(mockAllManagers);
        });

        it('call skillsService.getAllEmployeeSkills() once', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockAllSkills: ISkill[] = [mockSkill];

            jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            const spy = jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('subscribe to skillsService.getAllEmployeeSkills() observable and save the emitted value', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockAllSkills: ISkill[] = [mockSkill];

            const mockAllSkillsNames: string[] = [mockSkill.skill];

            jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            const spy = jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.allSkills).toEqual(mockAllSkillsNames);
        });

        it('filter skills', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockSkillAdmin: ISkill = {
                skillId: 'skill',
                skill: 'Admin'
            };

            const mockSkillManagement: ISkill = {
                skillId: 'skill',
                skill: 'Management'
            };

            const mockAllSkills: ISkill[] = [mockSkill, mockSkillAdmin, mockSkillManagement];

            jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            const spy = jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            // Act
            component.ngOnInit();

            // Assert
            expect(component.allSkills).toEqual([mockSkill.skill]);
        });

        it('call formBuilder.group() once', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockAllSkills: ISkill[] = [mockSkill];

            const mockUsersForm = 'users form';

            jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            const spy = jest.spyOn(formBuilder, 'group').mockReturnValue(mockUsersForm);

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('set the usersForm filed value', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            const mockAllManagers: IBasicUser[] = [mockManager];

            const mockSkill: ISkill = {
                skillId: 'skill',
                skill: 'Unit Testing'
            };

            const mockAllSkills: ISkill[] = [mockSkill];

            const usersFormConfigurations = {
                firstName: '',
                lastName: '',
                email: '',
                position: '',
                manager: '',
                userSkills: '',
                isManager: '',
                isAdmin: ''
            };

            jest.spyOn(usersService, 'getAllManagers').mockReturnValue(of(mockAllManagers));
            jest.spyOn(skillsService, 'getAllEmployeeSkills').mockReturnValue(of(mockAllSkills));

            const spy = jest.spyOn(formBuilder, 'group').mockReturnValue(usersFormConfigurations);

            // Act
            component.ngOnInit();

            // Assert
            expect(component.usersForm).toEqual(usersFormConfigurations);
        });
    });

    describe('disableSkillsIfUserHaveThem should', () => {

        it('return true, when user has the current skill', () => {

            // Arrange

            const skill = ('JavaScript');

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['JavaScript'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            // Act
            const result = component.disableSkillsIfUserHaveThem(skill);

            // Assert
            expect(result).toBeTruthy();
        });

        it('return false, when user has not the current skill', () => {

            // Arrange

            const skill = ('JavaScript');

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: [],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            // Act
            const result = component.disableSkillsIfUserHaveThem(skill);

            // Assert
            expect(result).toBeFalsy();
        });
    });

    describe('disableUserAsManagerIfHeIsOne should', () => {

        it('return true, when user is manager', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user'
            };

            component.allManagers = [mockManager];

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            // Act
            const result = component.disableUserAsManagerIfHeIsOne(mockManager);

            // Assert
            expect(result).toBeTruthy();
        });

        it('return true, when user is not manager', () => {

            // Arrange
            const mockManager: IBasicUser = {
                userId: 'string1',
                firstName: 'user',
                lastName: 'user'
            };

            component.allManagers = [mockManager];

            const mockUser: IUser = {
                userId: 'string',
                firstName: 'user',
                lastName: 'user',
                email: 'user@email.com',
                userSkills: ['Unit Testing'],
                position: 'Junior Developer',
                availableTime: 8,
                projects: [],
                employees: [],
                manager: null
            };

            component.user = mockUser;

            // Act
            const result = component.disableUserAsManagerIfHeIsOne(mockManager);

            // Assert
            expect(result).toBeFalsy();
        });
    });
});
