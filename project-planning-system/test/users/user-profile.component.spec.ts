import { UsersRoutingModule } from './../../src/app/users/users-routing.module';
import { SharedModule } from './../../src/app/shared/shared.module';
import { UsersModalComponent } from './../../src/app/users/users-modal/users-modal.component';
import { AllUsersComponent } from './../../src/app/users/all-users/all-users.component';
import { UserProfileComponent } from './../../src/app/users/user-profile/user-profile.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { UsersService } from './../../src/app/users/users.service';
import { AuthService } from './../../src/app/auth/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from './../../src/app/core/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';

describe('UserProfileComponent', () => {

    let usersService;
    let authService;
    let route;
    let router;
    let dialog;
    let notificationService;

    let fixture: ComponentFixture<UserProfileComponent>;
    let component: UserProfileComponent;

    beforeEach(async(() => {

        jest.clearAllMocks();

        usersService = {
            getIndividualUser() { /* empty */ },
            getIndividualUserDataPromise() { /* empty */ },
            createAdminOrManager() { /* empty */ },
            sendPasswordResetEmail() { /* empty */ },
            updateEmployee() { /* empty */ }
        };

        authService = {
            get getLoggedUser() {
                return of();
            }
        };

        route = {
            params() {}
        };

        router = {
            navigate() { /* empty */ }
        };

        dialog = {
            open() { /* empty */ }
        };

        notificationService = {
            success() { /* empty */ }
        };

        TestBed.configureTestingModule({

            declarations: [
                AllUsersComponent,
                UserProfileComponent,
                UsersModalComponent
            ],
            imports: [
                SharedModule,
                UsersRoutingModule,
                RouterTestingModule
            ],
            providers: [
                UsersService, AuthService, MatDialog, NotificationService
            ]
        })
            .overrideProvider(UsersService, { useValue: usersService })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(ActivatedRoute, { useValue: route })
            .overrideProvider(Router, { useValue: router })
            .overrideProvider(MatDialog, { useValue: dialog })
            .overrideProvider(NotificationService, { useValue: notificationService })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(UserProfileComponent);
                component = fixture.componentInstance;
            });
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    // describe('ngOnInit() should', () => {


    // });
});
