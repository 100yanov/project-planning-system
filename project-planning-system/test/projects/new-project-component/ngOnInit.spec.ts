import { NotificationService } from './../../../src/app/core/services/notification.service';
import { SkillsService } from './../../../src/app/skills/skills.service';
import { AuthService } from './../../../src/app/auth/auth.service';
import { ProjectsService } from './../../../src/app/projects/projects.service';
import { NewProjectComponent } from '../../../src/app/projects/new-project/new-project.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { AllProjectsComponent } from '../../../src/app/projects/all-projects/all-projects.component';
import { SingleProjectComponent } from '../../../src/app/projects/single-project/single-project.component';
import { NewProjectDescriptionComponent } from '../../../src/app/projects/new-project-description/new-project-description.component';
import { NewProjectSkillsComponent } from '../../../src/app/projects/new-project-skills/new-project-skills.component';
import { NewProjectEmployeesComponent } from '../../../src/app/projects/new-project-employees/new-project-employees.component';
import { IndividualProjectComponent } from '../../../src/app/projects/individual-project/individual-project.component';
import { SharedModule } from '../../../src/app/shared/shared.module';
import { ProjectsRoutingModule } from '../../../src/app/projects/projects-routing.module';
import { UsersService } from '../../../src/app/users/users.service';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

describe('NewProjectComponent', () => {
  let projectsService;
  let authService;
  let skillsService;
  let usersService;
  let notificationService;
  let dialog;

  let fixture: ComponentFixture<NewProjectComponent>;
  let component: NewProjectComponent;

  beforeEach(async(() => {
    projectsService = {
      getAllProjects() {
        /* empty */
      },
      getIndividualProject() {
        /* empty */
      },
      updateProject() {
        /* empty */
      },
      addProject() {
        /* empty */
      },
      checkProjectCompletionDays() {
        /* empty */
      },
    };

    authService = {
      get getLoggedUser() {
        return of();
      },
    };

    skillsService = {
      getAllEmployeeSkills() {
        /* empty */
      },
      createSkill() {
        /* empty */
      },
    };

    usersService = {
      getAllUsers() {
        /* empty */
      },
      createAdminOrManager() {
        /* empty */
      },
      sendPasswordResetEmail() {
        /* empty */
      },
      createEmployee() {
        /* empty */
      },
      getIndividualUserDataPromise() {
        /* empty */
      },
      updateEmployee() {
        /* empty */
      },
    };

    notificationService = {
      error() {
        /* empty */
      },
      success() {
        /* empty */
      },
    };

    dialog = {
      open() {
        /* empty */
      },
    };

    TestBed.configureTestingModule({
      declarations: [
        AllProjectsComponent,
        SingleProjectComponent,
        NewProjectComponent,
        NewProjectDescriptionComponent,
        NewProjectSkillsComponent,
        NewProjectEmployeesComponent,
        IndividualProjectComponent,
      ],
      imports: [SharedModule, ProjectsRoutingModule, RouterTestingModule],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        ProjectsService,
        AuthService,
        SkillsService,
        UsersService,
        MatDialog,
        NotificationService,
      ],
    })
      .overrideProvider(ProjectsService, { useValue: projectsService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(SkillsService, { useValue: skillsService })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .overrideProvider(MatDialog, { useValue: dialog })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(NewProjectComponent);
        component = fixture.componentInstance;
      });
  }));

  describe('ngOnInit should', () => {
    it('');
  });
});
