import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

export const createUser = functions
    .region('europe-west3')
    .https.onCall(async (data, context) => {
        return await admin.auth().createUser({
            email: data.email,
            password: data.password,
            displayName: data.displayName
        });
    });