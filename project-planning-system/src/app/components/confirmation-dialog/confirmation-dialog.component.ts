import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  public confirmation: {
    confirmed: boolean,
  };

 public constructor(
   @Inject(MAT_DIALOG_DATA) public data: {
    confirmed: boolean,
  }
   ) {}

  ngOnInit() {
    this.confirmation = this.data;
  }

  public confirmed() {
    this.confirmation.confirmed = true;
  }

  public denied() {
    this.confirmation.confirmed = false;
  }
}
