import { NotificationService } from './../../core/services/notification.service';
import { AuthService } from './../../auth/auth.service';
import { IUser } from '../../common/interfaces/user';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss'],
})
export class NavigationMenuComponent implements OnInit {
  public loggedUser: IUser;
  public isLoggedUserAdmin = false;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) {}

  public ngOnInit(): void {
    this.authService.getLoggedUser.subscribe(
      (user) => {
        this.loggedUser = user;

        if (this.loggedUser && this.loggedUser.userSkills.includes('Admin')) {
          this.isLoggedUserAdmin = true;
        }
      },
      (err) =>
        this.notificationService.error(
          'Something went wrong, please try again!'
        )
    );
  }

  public async logout(): Promise<void> {
    try {
      await this.authService.signOut();

      this.router.navigate(['login']);
    } catch (error) {
      this.notificationService.error('Something went wrong, please try again!');
    }
  }
}
