import { NotificationService } from './../../core/services/notification.service';
import { CreateSkillComponent } from './../create-skill/create-skill.component';
import { Router } from '@angular/router';
import { ISkill } from './../../common/interfaces/skill';
import { SkillsService } from './../skills.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-all-skills',
  templateUrl: './all-skills.component.html',
  styleUrls: ['./all-skills.component.scss']
})
export class AllSkillsComponent implements OnInit {

  public skills: ISkill[];

  public skillsNames: string[];

  public fileNameDialogRef: MatDialogRef<CreateSkillComponent>;

  public constructor(
    private readonly skillsService: SkillsService,
    private readonly router: Router,
    private readonly dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) { }

  public ngOnInit(): void {

    this.skillsService.getAllEmployeeSkills().subscribe(
      (skills) => {
        this.skills = skills;

        this.skillsNames = this.skills.reduce((acc, currVal) => {

          if (currVal.skill !== 'Admin' && currVal.skill !== 'Management') {
            acc.push(currVal.skill);
          }

          return acc;
        }, []);

        this.skillsNames = this.skillsNames.sort((a, b) => {
          if (a < b) {
            return -1;
          }
          if (a > b) {
            return 1;
          }
          return 0;
        });
      },
      (err) =>
      this.notificationService.error(
        'Something went wrong, please try again!'
      )
    );
  }

  public openAddNewSkillDialog() {

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.autoFocus = true;
    matDialogConfig.disableClose = true;
    matDialogConfig.maxHeight = '80vw';
    matDialogConfig.maxWidth = '80vw';
    matDialogConfig.width = '40vw';

    this.fileNameDialogRef = this.dialog.open(CreateSkillComponent, matDialogConfig);
  }
}
