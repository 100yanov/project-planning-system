import { NgModule } from '@angular/core';
import { SkillsRoutingModule } from './skills-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AllSkillsComponent } from './all-skills/all-skills.component';
import { CreateSkillComponent } from './create-skill/create-skill.component';


@NgModule({
  declarations: [
    AllSkillsComponent,
    CreateSkillComponent
  ],
  imports: [
    SkillsRoutingModule,
    SharedModule
  ]
})
export class SkillsModule { }
