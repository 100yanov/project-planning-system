import { ISkill } from './../common/interfaces/skill';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class SkillsService {
  constructor(
    private readonly afs: AngularFirestore
    ) {}

  public getAllEmployeeSkills(): Observable<ISkill[]> {

    return this.afs.collection<ISkill>('Skills').valueChanges();
  }

  public async createSkill(skill: ISkill): Promise<void> {

    const skillId = this.afs.createId();

    await this.afs.collection('Skills').doc<ISkill>(skillId).set({ ...skill, skillId });
  }
}
