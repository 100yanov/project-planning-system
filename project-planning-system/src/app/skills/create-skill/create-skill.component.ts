import { NotificationService } from './../../core/services/notification.service';
import { SkillsService } from './../skills.service';
import { ISkill } from './../../common/interfaces/skill';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-skill',
  templateUrl: './create-skill.component.html',
  styleUrls: ['./create-skill.component.scss']
})
export class CreateSkillComponent implements OnInit {

  public skillsForm: FormGroup;

  public constructor(
    private readonly skillsService: SkillsService,
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService
  ) { }

  public ngOnInit(): void {

    this.skillsForm = this.formBuilder.group({
      skill: [
        '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(48)]
      ],
    });
  }

  public async createSkill(skill: ISkill): Promise<void> {

    try {
      await this.skillsService.createSkill(skill);

      this.notificationService.success('Successfully created skill');
    } catch (error) {
      this.notificationService.error('Something went wrong, please try again!');
    }
  }
}
