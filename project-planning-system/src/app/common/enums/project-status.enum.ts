export enum ProjectStatus {
  inProgress = 'In Progress',
  finished = 'Finished',
  never = 'Never',
}
