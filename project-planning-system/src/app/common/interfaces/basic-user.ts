export interface IBasicUser {
  userId: string;
  firstName: string;
  lastName: string;
}
