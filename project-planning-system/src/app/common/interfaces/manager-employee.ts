export interface IManagerEmployee {
    userId: string;
    firstName: string;
    lastName: string;
    email: string;
    position: string;
    availableTime: number;
  }
