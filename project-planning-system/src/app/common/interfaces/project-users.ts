import { ISkillHoursPerDay } from './skill-hour-per-day';
export interface IProjectUsers {
  userId: string;
  firstName: string;
  lastName: string;
  availableTime: number;
  skillsHoursPerDay: ISkillHoursPerDay[];
}
