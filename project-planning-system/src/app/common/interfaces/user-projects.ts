import { ISkillHoursPerDay } from './skill-hour-per-day';
export interface IUserProjects {
  projectId: string;
  userSkillsInProject: ISkillHoursPerDay[];
  name: string;
  status: string;
}
