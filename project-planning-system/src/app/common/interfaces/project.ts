import { IProjectUsers } from './project-users';
import { ISkillTotalHours } from './skill-total-hours';
import { IBasicUser } from './basic-user';
import { ISkillHoursPerDay } from './skill-hour-per-day';
import * as moment from 'moment';
import { Moment } from 'moment';

export interface IProject {
  name: string;
  fulfilmentTarget: number;
  description: string;
  dailySkills: ISkillHoursPerDay[];
  totalSkills: ISkillTotalHours[];
  status: string;
  employees: IProjectUsers[];
  manager: IBasicUser;
  startDate: string;
  projectId: string;
  checkDate: string;
}
