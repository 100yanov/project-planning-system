export interface ISkillHoursPerDay {
  skill: string;
  hoursPerDay: number;
}
