export interface IProjectDescription {
  name: string;
  description: string;
  targetDays: number;
}
