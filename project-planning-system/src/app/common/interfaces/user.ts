import { IManagerEmployee } from './manager-employee';
import { IBasicUser } from './basic-user';
import { IUserProjects } from './user-projects';

export interface IUser {
  userId: string;
  firstName: string;
  lastName: string;
  email: string;
  userSkills: string[];
  position: string;
  availableTime: number;
  projects: IUserProjects[];
  employees: IManagerEmployee[];
  manager: IBasicUser | null;
}
