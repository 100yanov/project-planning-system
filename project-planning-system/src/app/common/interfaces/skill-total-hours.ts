export interface ISkillTotalHours {
  skill: string;
  totalHours: number;
  hoursLeft: number;
}
