import { IBasicUser } from './basic-user';

export interface ISubmitUser {
    firstName: string;
    lastName: string;
    email: string;
    position: string;
    manager: IBasicUser | null;
    userSkills: string[];
  }
