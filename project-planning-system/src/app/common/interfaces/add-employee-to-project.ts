import { IProjectUsers } from './project-users';
import { ISkillHoursPerDay } from './skill-hour-per-day';

export interface IAddEmployeeToProject {
  employeeToAddToProject: IProjectUsers;
  skillAndHours: ISkillHoursPerDay;
}
