import { NotificationService } from './../../core/services/notification.service';
import { UsersService } from './../../users/users.service';
import { ProjectStatus } from './../../common/enums/project-status.enum';
import { IUserProjects } from '../../common/interfaces/user-projects';
import { AuthService } from './../../auth/auth.service';
import { IUser } from './../../common/interfaces/user';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public loggedUser: IUser;

  public users: IUser[];

  public managerEmployees: IUser[];

  public managerProjects: IUserProjects[];

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService
  ) { }

  public ngOnInit(): void {

    this.authService.getLoggedUser.subscribe((user) => {
      this.loggedUser = user;

      if (this.loggedUser) {

        const projects = this.loggedUser.projects;
        this.managerProjects = projects && projects.filter(
          (project) => project.status === ProjectStatus.inProgress
        );
      }
    },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );

    this.usersService.getAllUsers().subscribe(
      (data) => {
        this.users = data;

        this.managerEmployees = this.users && this.users.filter(user => {
          if (user.manager && this.loggedUser) {
            return user.manager.userId === this.loggedUser.userId;
          }
        });
      },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );
  }
}
