import { IUser } from './../common/interfaces/user';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private loggedUser: IUser | null;

  private readonly loggedUserSubject$ = new BehaviorSubject<IUser>(
    this.loggedUser
  );

  public constructor(
    private readonly angularFireAuth: AngularFireAuth,
    private readonly angularFirestore: AngularFirestore
  ) {
    this.angularFireAuth.authState.subscribe((user) => {
      if (!user) {
        this.loggedUser = null;
        this.loggedUserSubject$.next(this.loggedUser);
        return;
      }

      this.angularFirestore
        .collection('Users', (ref) => ref.where('email', '==', user.email))
        .get()
        .subscribe((snapshot) => {
          snapshot.docs.forEach((doc) => {
            const data = doc.data();
            this.loggedUser = data && {
              userId: data.userId,
              firstName: data.firstName,
              lastName: data.lastName,
              email: data.email,
              userSkills: data.userSkills,
              position: data.position,
              availableTime: data.availableTime,
              projects: data.projects,
              employees: data.employees,
              manager: data.manager,
            };
            this.loggedUserSubject$.next(this.loggedUser);
          });
        });
    });
  }

  public get getLoggedUser(): Observable<IUser> {
    return this.loggedUserSubject$.asObservable();
  }

  public async signIn(email: string, password: string): Promise<void> {
    await this.angularFireAuth.signInWithEmailAndPassword(email, password);
  }

  public async signOut(): Promise<void> {
    await this.angularFireAuth.signOut();
  }
}
