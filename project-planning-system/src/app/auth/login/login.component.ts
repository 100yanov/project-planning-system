import { NotificationService } from './../../core/services/notification.service';
import { IUser } from './../../common/interfaces/user';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  public loggedUser: IUser;

  public hide = true;

  public constructor(
    private readonly form: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) { }

  public ngOnInit(): void {

    this.authService.getLoggedUser.subscribe(
      (user) => {
        this.loggedUser = user;

        if (this.loggedUser) {
          this.router.navigate(['dashboard']);
        }
      },  (err) =>
      this.notificationService.error('Something went wrong, please try again!')
    );

    this.loginForm = this.form.group({
      email: [
        '',
        [Validators.required, Validators.email]
      ],
      password: [
        '',
        [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/)]
      ]
    });
  }

  public async login(): Promise<void> {

    try {
      await this.authService.signIn(
        this.loginForm.get('email').value,
        this.loginForm.get('password').value,
      );
    } catch (error) {
      this.notificationService.error('Invalid email or password');
      return;
    }

    this.notificationService.success('Login successful');
    this.router.navigate(['dashboard']);
  }

  public loginWithEnter(event): void {
    if (event.which === 13) {
      this.login();
    }
  }
}
