import { Component, OnInit } from '@angular/core';
import { IUser } from './common/interfaces/user';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

 public title = 'project-planning-system';

  public loggedUser: IUser;

  public constructor(
    public readonly authService: AuthService
  ) { }

  public ngOnInit(): void {

    this.authService.getLoggedUser.subscribe((user) => {
      this.loggedUser = user;
    });
  }
}
