import { NotificationService } from './../../core/services/notification.service';
import { ISubmitUser } from './../../common/interfaces/submit-user';
import { ProjectStatus } from './../../common/enums/project-status.enum';
import { SkillsService } from './../../skills/skills.service';
import { UsersService } from './../users.service';
import { IBasicUser } from '../../common/interfaces/basic-user';
import { IUser } from './../../common/interfaces/user';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-users-modal',
  templateUrl: './users-modal.component.html',
  styleUrls: ['./users-modal.component.scss']
})
export class UsersModalComponent implements OnInit {

  public user: IUser | undefined;

  public usersForm: FormGroup;

  @Output()
  public userSubmited: EventEmitter<ISubmitUser> = new EventEmitter<ISubmitUser>();

  public allManagers: IBasicUser[];

  public allSkills: string[];

  public constructor(
    private readonly usersService: UsersService,
    private readonly skillsService: SkillsService,
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService
  ) { }

  public ngOnInit(): void {

    this.usersService.getAllManagers().subscribe(
      (users) => {
        this.allManagers = users.map(user => ({
          userId: user.userId,
          firstName: user.firstName,
          lastName: user.lastName
        }));

        if (this.user && this.user.manager) {

          this.usersForm.get('manager').setValue(this.allManagers.find((manager) => {

            return manager.userId === this.user.manager.userId;
          }));
        }

        if (this.user && !this.user.manager) {

          this.usersForm.get('manager').setValue('Self-managed');
        }
      },
      (err) =>
      this.notificationService.error('Something went wrong, please try again!')
    );

    this.skillsService.getAllEmployeeSkills().subscribe(
      (skills) => {
        this.allSkills = skills.map(skill => skill.skill).filter(skill => {
          return skill !== 'Admin' && skill !== 'Management';
        });
      },
      (err) =>
      this.notificationService.error('Something went wrong, please try again!')
    );

    const isManager = this.user && this.user.userSkills.includes('Management');

    const hasProjects = this.user && this.user.projects &&
      this.user.projects.some((p) => p.status === ProjectStatus.inProgress);

    const isAdmin = this.user && this.user.userSkills.includes('Admin');

    this.usersForm = this.formBuilder.group({
      firstName: [
        this.user !== undefined ? { value: this.user.firstName, disabled: true } : '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(128)]
      ],
      lastName: [
        this.user !== undefined ? { value: this.user.lastName, disabled: true } : '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(128)]
      ],
      email: [
        this.user !== undefined ? { value: this.user.email, disabled: true } : '',
        [Validators.required, Validators.email]
      ],
      position: [
        this.user !== undefined ? this.user.position : '',
        [Validators.required]
      ],
      manager: new FormControl(
        '',
        [Validators.required]
      ),
      userSkills: new FormControl(
        {
          value: this.user !== undefined
            ? this.user.userSkills
            : [],
          disabled: isManager
        }
      ),
      isManager: new FormControl(
        { value: isManager, disabled: isManager || hasProjects }
      ),
      isAdmin: new FormControl(
        { value: isAdmin, disabled: isAdmin }
      ),
    });
  }

  public onIsManagerChanged(): void {

    if (this.usersForm.get('isManager').value) {
      this.usersForm.get('userSkills').disable();
      this.usersForm.get('userSkills').reset();
    } else {
      this.usersForm.get('userSkills').enable();
    }
  }

  public disableSkillsIfUserHaveThem(skill: string): boolean {

    return this.user && this.user.userSkills.some((s) => s === skill);
  }

  public disableUserAsManagerIfHeIsOne(manager: IBasicUser): boolean {

    return this.user && this.allManagers.some(() => this.user.userId === manager.userId);
  }

  public onSubmit(employee: ISubmitUser): void {

    if (this.usersForm.get('isManager').value) {

      employee.userSkills = ['Management'];

      if (this.usersForm.get('isAdmin').value) {

        employee.userSkills.unshift('Admin');
      }

      if (this.usersForm.get('manager').value === 'Self-managed') {

        employee.manager = null;
      }
    }

    this.userSubmited.emit({
      firstName: employee.firstName || (this.user && this.user.firstName),
      lastName: employee.lastName || (this.user && this.user.lastName),
      email: employee.email || (this.user && this.user.email),
      position: employee.position,
      manager: employee.manager,
      userSkills: employee.userSkills
    });
  }
}
