import { IManagerEmployee } from '../../common/interfaces/manager-employee';
import { NotificationService } from './../../core/services/notification.service';
import { ISubmitUser } from './../../common/interfaces/submit-user';
import { Router } from '@angular/router';
import { UsersModalComponent } from './../users-modal/users-modal.component';
import { AuthService } from './../../auth/auth.service';
import { IUser } from './../../common/interfaces/user';
import { UsersService } from './../users.service';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {

  public users: IUser[];
  public usersToShow: MatTableDataSource<IUser> = new MatTableDataSource([]);
  private directSubordinates: IUser[];

  public user: IUser;

  public loggedUser: IUser;
  public isLoggedUserAdmin = false;
  public checked = false;
  public readonly managerDefaultValue = 'Self-managed';

  public filterName: string = undefined;
  public filterEmail: string = undefined;
  public filterPosition: string = undefined;
  public filterManager: string = undefined;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  public fileNameDialogRef: MatDialogRef<UsersModalComponent>;

  public constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly dialog: MatDialog,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
  ) { }

  public ngOnInit(): void {

    this.usersToShow.sort = this.sort;
    this.usersToShow.paginator = this.paginator;

    this.usersService.getAllUsers().subscribe(
      (data) => {
        this.users = data;
        this.usersToShow.data = this.users;
      },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );

    this.authService.getLoggedUser.subscribe(user => {

      this.loggedUser = user;

      if (this.loggedUser && this.loggedUser.userSkills.includes('Admin')) {

        this.isLoggedUserAdmin = true;
      }
    },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );
  }

  public openAddNewEmployeeDialog() {

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.autoFocus = true;
    matDialogConfig.disableClose = true;
    matDialogConfig.maxHeight = '80vw';
    matDialogConfig.maxWidth = '80vw';
    matDialogConfig.width = '60vw';

    this.fileNameDialogRef = this.dialog.open(UsersModalComponent, matDialogConfig);

    this.fileNameDialogRef.componentInstance.userSubmited.subscribe(async (employee: ISubmitUser) => {

      const isEmailAlreadyExist = this.users.find((user) => user.email === employee.email);

      if (isEmailAlreadyExist) {

        this.notificationService.error('Employee with such email already exist!');

        return;
      }

      const isManager: boolean = employee.userSkills.includes('Management');

      const userToAdd: IUser = { ...this.user, ...employee };

      userToAdd.employees = userToAdd.employees || [];
      userToAdd.projects = userToAdd.projects || [];

      if (isManager) {
        await this.usersService.createAdminOrManager(userToAdd);
        await this.usersService.sendPasswordResetEmail(employee.email);
      }

      const newUserId = await this.usersService.createEmployee(userToAdd);

      const newUser: IUser = await this.usersService.getIndividualUserDataPromise(newUserId);

      if (newUser.manager) {

        const newEmployeeManager: IUser = this.users.find((u) => u.userId === newUser.manager.userId);

        const managersNewEmployee: IManagerEmployee = {
          userId: newUser.userId,
          firstName: newUser.firstName,
          lastName: newUser.lastName,
          email: newUser.email,
          position: newUser.position,
          availableTime: newUser.availableTime,
        };

        if (!newEmployeeManager.employees) {
          newEmployeeManager.employees = [];
        }

        newEmployeeManager.employees = [...newEmployeeManager.employees, managersNewEmployee];

        await this.usersService.updateEmployee(newEmployeeManager);
      }

      this.notificationService.success('Successfully created employee');
    },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );
  }

  public filterDirectSubordinatesEvent(event: Event): void {

    this.filterName = undefined;
    this.filterEmail = undefined;
    this.filterPosition = undefined;
    this.filterManager = undefined;

    if (event) {
      this.usersToShow.data = this.filterDirectSubordinates();
    } else {
      this.usersToShow.data = this.users;
    }
  }

  public filterByField(field: string, filter: string): void {

    let usersToFilter: IUser[];

    if (this.checked) {
      usersToFilter = this.directSubordinates;
    } else {
      usersToFilter = this.users;
    }

    const splitFields = field.split(' || ');

    this.usersToShow.data = usersToFilter.filter(user => {

      for (const currentField of splitFields) {
        if (user[currentField] && user[currentField].toLowerCase().includes(filter.toLowerCase())) {

          return true;
        }
      }

      return false;
    });
  }

  public filterByManager(filter: string, defaultValue: string): void {

    let usersToFilter: IUser[];

    if (this.checked) {
      usersToFilter = this.directSubordinates;
    } else {
      usersToFilter = this.users;
    }

    this.usersToShow.data = usersToFilter.filter(user => {

      if (user.manager) {

        return user.manager.firstName && user.manager.firstName.toLowerCase().includes(filter.toLowerCase());
      }

      return defaultValue.toLowerCase().includes(filter.toLowerCase());
    });
  }

  private filterDirectSubordinates(): IUser[] {

    return this.directSubordinates = this.users && this.users.filter(user => {
      if (user.manager) {
        return user.manager.userId === this.loggedUser.userId;
      }
    });
  }
}
