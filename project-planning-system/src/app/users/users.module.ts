import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { UsersRoutingModule } from './users-routing.module';
import { AllUsersComponent } from './all-users/all-users.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UsersModalComponent } from './users-modal/users-modal.component';

@NgModule({
  declarations: [
    AllUsersComponent,
    UserProfileComponent,
    UsersModalComponent
  ],
  imports: [
    UsersRoutingModule,
    SharedModule
  ]
})
export class UsersModule { }
