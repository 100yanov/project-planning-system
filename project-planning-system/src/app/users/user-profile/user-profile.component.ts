import { IManagerEmployee } from '../../common/interfaces/manager-employee';
import { NotificationService } from './../../core/services/notification.service';
import { ISubmitUser } from './../../common/interfaces/submit-user';
import { UsersModalComponent } from './../users-modal/users-modal.component';
import { AuthService } from './../../auth/auth.service';
import { UsersService } from './../users.service';
import { IUser } from './../../common/interfaces/user';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogConfig, MatDialogRef, MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  @Input()
  public user: IUser;

  public loggedUser: IUser;
  public isLoggedUserAdmin = false;

  public fileNameDialogRef: MatDialogRef<UsersModalComponent>;

  public constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) { }

  public ngOnInit(): void {

    this.route.params.subscribe(params => {

      this.usersService.getIndividualUser(params.userId)
        .subscribe(
          (foundUser) => {
            this.user = foundUser;

            if (!this.user) {
              this.router.navigate(['/not-found']);
            }
          },
          () => this.router.navigate(['/server-error'])
        );
    },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );

    this.authService.getLoggedUser.subscribe(user => {

      this.loggedUser = user;

      if (this.loggedUser && this.loggedUser.userSkills.includes('Admin')) {

        this.isLoggedUserAdmin = true;
      }
    },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );
  }

  public openUpdateEmployeeDialog() {

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.autoFocus = false;
    matDialogConfig.disableClose = true;
    matDialogConfig.maxHeight = '80vw';
    matDialogConfig.maxWidth = '80vw';
    matDialogConfig.width = '60vw';

    this.fileNameDialogRef = this.dialog.open(UsersModalComponent, matDialogConfig);

    this.fileNameDialogRef.componentInstance.user = { ...this.user };

    this.fileNameDialogRef.componentInstance.userSubmited.subscribe(async (employee: ISubmitUser) => {

      let oldEmployeeManager: IUser;
      if (this.user.manager) {
        oldEmployeeManager = await this.usersService.getIndividualUserDataPromise(this.user.manager.userId);
      }

      const isManager: boolean = employee.userSkills.includes('Management');

      if (!this.user.userSkills.includes('Management') && isManager) {
        await this.usersService.createAdminOrManager({ ...this.user, ...employee });
        await this.usersService.sendPasswordResetEmail(this.user.email);
      }

      await this.usersService.updateEmployee({ ...this.user, ...employee });

      const updatedUser: IUser = await this.usersService.getIndividualUserDataPromise(this.user.userId);

      if (oldEmployeeManager) {

        if (!updatedUser.manager || updatedUser.manager.userId !== oldEmployeeManager.userId) {

          oldEmployeeManager.employees = oldEmployeeManager.employees.reduce((acc, currVal) => {

            if (currVal.userId !== updatedUser.userId) {

              acc.push(currVal);
            }

            return acc;
          }, []);
        }

        this.usersService.updateEmployee(oldEmployeeManager);
      }

      if (updatedUser.manager) {

        const newEmployeeManager: IUser = await this.usersService.getIndividualUserDataPromise(updatedUser.manager.userId);

        if (!newEmployeeManager.employees || !newEmployeeManager.employees.find(e => e.userId === updatedUser.userId)) {

          const managersNewEmployee: IManagerEmployee = {
            userId: updatedUser.userId,
            firstName: updatedUser.firstName,
            lastName: updatedUser.lastName,
            email: updatedUser.email,
            position: updatedUser.position,
            availableTime: updatedUser.availableTime,
          };

          if (!newEmployeeManager.employees) {
            newEmployeeManager.employees = [];
          }

          newEmployeeManager.employees = [...newEmployeeManager.employees, managersNewEmployee];

          await this.usersService.updateEmployee(newEmployeeManager);
        }
      }

      this.notificationService.success('Successfully updated employee');
    },
      (err) =>
        this.notificationService.error('Something went wrong, please try again!')
    );
  }
}
