import { AngularFireAuth } from '@angular/fire/auth';
import { IUser } from './../common/interfaces/user';
import { Observable } from 'rxjs';
import { AngularFirestore, DocumentSnapshot } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/functions';
import { IUserProjects } from '../common/interfaces/user-projects';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  public constructor(
    private readonly angularFirestore: AngularFirestore,
    private readonly angularFireAuth: AngularFireAuth,
    private angularFireFunctions: AngularFireFunctions
  ) { }

  public getAllUsers(): Observable<IUser[]> {
    return this.angularFirestore.collection<IUser>('Users').valueChanges();
  }

  public getAllManagers(): Observable<IUser[]> {
    return this.angularFirestore
      .collection<IUser>('Users', (ref) =>
        ref.where('userSkills', 'array-contains', 'Management')
      )
      .valueChanges();
  }

  public getIndividualUser(userId: string): Observable<IUser> {
    return this.angularFirestore
      .collection('Users')
      .doc<IUser>(userId)
      .valueChanges();
  }

  public getIndividualUserDataPromise(userId: string): Promise<IUser> {

    return this.angularFirestore
      .collection('Users')
      .doc<IUser>(userId)
      .get().toPromise().then(snapshot => {
        const data = snapshot.data();

        return {
          userId: data.userId,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          userSkills: data.userSkills,
          position: data.position,
          availableTime: data.availableTime,
          projects: data.projects,
          employees: data.employees,
          manager: data.manager
        };
      });
    }

  public getIndividualUserPromise(userId: string): Promise<IUser> {
    return this.angularFirestore
      .collection('Users')
      .doc<IUser>(userId)
      .get()
      .toPromise()
      .then((doc: DocumentSnapshot<IUser>) => doc.data());
  }

  public getIndividualUserDoc(userId: string) {
    return this.angularFirestore.collection('Users').doc<IUser>(userId);
  }

  public async createEmployee(userData: IUser): Promise<string> {
    const userId = this.angularFirestore.createId();

    await this.angularFirestore.collection('Users').doc<IUser>(userId).set({ ...userData, userId, availableTime: 8 });

    return userId;
  }

  public async sendPasswordResetEmail(email: string): Promise<void> {
    await this.angularFireAuth.sendPasswordResetEmail(email);
  }

  public async createAdminOrManager(userData: IUser): Promise<void> {
    const password = Math.random().toString(36).slice(-8);

    // TODO remove this
    console.log(password);

    const createUserFn = this.angularFireFunctions.httpsCallable('createUser');

    await createUserFn({
      displayName: userData.firstName + ' ' + userData.lastName,
      email: userData.email,
      password,
    }).toPromise();
  }

  public async updateEmployee(userData: IUser): Promise<void> {
    this.angularFirestore
      .collection('Users')
      .doc<IUser>(userData.userId)
      .update(userData);
  }

  public async updateEmployeeAfterProject(userData: IUser): Promise<void> {
    this.angularFirestore
      .collection('Users')
      .doc<IUser>(userData.userId)
      .set(userData);
  }

  public updateEmployeeProjects(
    employeeId: string,
    project: IUserProjects,
    availableTime: number
  ) {
    this.getIndividualUserDoc(employeeId).ref.update({
      projects: firebase.firestore.FieldValue.arrayUnion(project),
      availableTime
    });
  }
}
