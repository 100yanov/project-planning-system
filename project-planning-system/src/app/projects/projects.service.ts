import { ProjectStatus } from './../common/enums/project-status.enum';
import { UsersService } from './../users/users.service';
import { IProject } from './../common/interfaces/project';
import { IUserProjects } from '../common/interfaces/user-projects';
import { Observable } from 'rxjs';
import { AngularFirestore, DocumentSnapshot } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  constructor(
    private readonly angularFirestore: AngularFirestore,
    private readonly usersService: UsersService
  ) {}

  public getAllProjects(): Observable<IProject[]> {
    return this.angularFirestore
      .collection<IProject>('Projects')
      .valueChanges();
  }

  public async getIndividualProject(projectId: string): Promise<IProject> {
    return this.angularFirestore
      .collection('Projects')
      .doc<IProject>(projectId)
      .get()
      .toPromise()
      .then((doc: DocumentSnapshot<IProject>) => doc.data());
  }

  public async updateProject(project: IProject): Promise<void> {
    this.angularFirestore
      .collection('Projects')
      .doc<IProject>(project.projectId)
      .set(project);
  }

  public async addProject(project: IProject): Promise<void> {
    let projectId: string;

    project.projectId
      ? (projectId = project.projectId)
      : (projectId = this.angularFirestore.createId());

    this.angularFirestore
      .collection('Projects')
      .doc<IProject>(projectId)
      .set({ ...project, projectId })
      .then(() => {
        project.employees.forEach((e) => {
          const userProject: IUserProjects = {
            projectId,
            userSkillsInProject: e.skillsHoursPerDay,
            name: project.name,
            status: project.status,
          };
          this.usersService.updateEmployeeProjects(
            e.userId,
            userProject,
            e.availableTime
          );
        });
      });
  }

  public checkProjectCompletionDays(project: IProject): number | string {
    const daysToWorkWithSkill = [];

    if (
      project.status === ProjectStatus.finished ||
      project.status === ProjectStatus.never
    ) {
      return ProjectStatus.finished;
    }

    project.totalSkills.forEach((ts) => {
      const dailySkill = project.dailySkills.find(
        (ds) => ds.skill === ts.skill
      );
      daysToWorkWithSkill.push(ts.hoursLeft / dailySkill.hoursPerDay);
    });

    if (daysToWorkWithSkill.includes(Infinity)) {
      return ProjectStatus.never;
    } else {
      let result = daysToWorkWithSkill.reduce((acc, days) => {
        if (days > acc) {
          return (acc = days);
        }
        return acc;
      }, 0);

      Number.isInteger(result) ? result = result : (result = result.toFixed(2));

      return `in ${result} days`;
    }
  }

  public getCompletionDays(project: IProject) {
    const daysToWorkWithSkill = [];

    if (
      project.status === ProjectStatus.finished ||
      project.status === ProjectStatus.never
    ) {
      return ProjectStatus.finished;
    }

    project.totalSkills.forEach((ts) => {
      const dailySkill = project.dailySkills.find(
        (ds) => ds.skill === ts.skill
      );
      daysToWorkWithSkill.push(ts.hoursLeft / dailySkill.hoursPerDay);
    });

    if (daysToWorkWithSkill.includes(Infinity)) {
      return ProjectStatus.never;
    } else {
      const result = daysToWorkWithSkill
        .reduce((acc, days) => {
          if (days > acc) {
            return (acc = days);
          }
          return acc;
        }, 0)
        .toFixed(2);

      return result;
    }
  }
}
