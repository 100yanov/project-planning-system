import { IProject } from './../../common/interfaces/project';
import { NotificationService } from './../../core/services/notification.service';
import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { IProjectDescription } from '../../common/interfaces/project-description';

@Component({
  selector: 'app-new-project-description',
  templateUrl: './new-project-description.component.html',
  styleUrls: ['./new-project-description.component.scss'],
})
export class NewProjectDescriptionComponent implements OnInit {
  @Output()
  public projectInfo = new EventEmitter();
  @Input()
  public newProjectInfo: IProject;
  public addInfoBtnValue = 'Add Info';

  constructor(private readonly notificationService: NotificationService) {}

  ngOnInit() {
    this.newProjectInfo.name
      ? (this.addInfoBtnValue = 'Edit Info')
      : (this.addInfoBtnValue = 'Add Info');
  }

  public addInfo(name: string, description: string, targetDays: number) {
    targetDays = +targetDays;

    if (!name || !description) {
      this.notificationService.error('Invalid project info!');
      return;
    }

    if (isNaN(targetDays) || targetDays <= 0) {
      this.notificationService.error(
        'Target Days should be valid positive number'
      );
      return;
    }

    const info: IProjectDescription = { name, description, targetDays };
    this.projectInfo.emit(info);
    this.addInfoBtnValue = 'Edit Info';
  }
}
