import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProjectDescriptionComponent } from './new-project-description.component';

describe('NewProjectDescriptionComponent', () => {
  let component: NewProjectDescriptionComponent;
  let fixture: ComponentFixture<NewProjectDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewProjectDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProjectDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
