import { IBasicUser } from '../../common/interfaces/basic-user';
import { IProject } from './../../common/interfaces/project';
import { NewProjectComponent } from './../new-project/new-project.component';
import { AuthService } from './../../auth/auth.service';
import { ProjectsService } from './../projects.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IUser } from '../../common/interfaces/user';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-all-projects',
  templateUrl: './all-projects.component.html',
  styleUrls: ['./all-projects.component.scss'],
})
export class AllProjectsComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public loggedUser: IUser;
  public projects: IProject[];
  public projectsToShow: MatTableDataSource<IProject> = new MatTableDataSource(
    []
  );
  private myProjects: IProject[];
  public isMyProjectsChecked = false;
  public manager: IBasicUser;

  public filterName: string = undefined;
  public filterDescription: string = undefined;
  public filterStatus: string = undefined;
  public filterManager: string = undefined;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private readonly projectsService: ProjectsService,
    private readonly authService: AuthService,
    private dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.projectsToShow.sort = this.sort;
    this.projectsToShow.paginator = this.paginator;

    this.loggedUserSubscription = this.authService.getLoggedUser.subscribe(
      (user) => (this.loggedUser = user)
    );

    this.projectsService.getAllProjects().subscribe(
      (p) => {
        this.projects = p;
        this.projectsToShow.data = this.projects;
      },
      (err) =>
        this.notificationService.error(
          'Something went wrong, please try again!'
        )
    );
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }

  public filterMyProjectsEvent(event: Event): void {
    this.filterName = undefined;
    this.filterDescription = undefined;
    this.filterStatus = undefined;
    this.filterManager = undefined;

    if (event) {
      this.projectsToShow.data = this.filterMyProjects();
    } else {
      this.projectsToShow.data = this.projects;
    }
  }

  public filterByField(field: string, filter: string): void {
    let projectsToFilter: IProject[];

    if (this.isMyProjectsChecked) {
      projectsToFilter = this.myProjects;
    } else {
      projectsToFilter = this.projects;
    }

    this.projectsToShow.data = projectsToFilter.filter((project) => {
      return project[field].toLowerCase().includes(filter.toLowerCase());
    });
  }

  public filterByManager(filter: string): void {
    let projectsToFilter: IProject[];

    if (this.isMyProjectsChecked) {
      projectsToFilter = this.myProjects;
    } else {
      projectsToFilter = this.projects;
    }

    this.projectsToShow.data = projectsToFilter.filter((project) => {
      if (project.manager) {
        return (
          project.manager.firstName &&
          project.manager.firstName.toLowerCase().includes(filter.toLowerCase())
        );
      }
    });
  }

  public onCreateNewProjectClick() {
    const project: IProject = this.initializeNewProject();

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.disableClose = true;
    dialogConfig.width = '80%';
    dialogConfig.height = '80%';
    dialogConfig.data = {
      project,
    };

    const dialogRef = this.dialog.open(NewProjectComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      async (newProject: IProject) => {
        if (!newProject) {
          return;
        }

        if (
          !newProject.name ||
          !newProject.description ||
          !newProject.fulfilmentTarget
        ) {
          this.notificationService.error(
            'Something went wrong, please try again!'
          );
          return;
        }

        project.employees = project.employees.filter(
          (e) => e.skillsHoursPerDay.length !== 0
        );

        if (!newProject.manager.userId) {
          newProject.manager = ({
            userId: this.loggedUser.userId,
            firstName: this.loggedUser.firstName,
            lastName: this.loggedUser.lastName
          });
        }

        await this.projectsService.addProject(newProject);
        this.notificationService.success(
          'Project has been successfully created!'
        );
      },
      (err) =>
        this.notificationService.error(
          'Something went wrong, please try again!'
        )
    );
  }

  public initializeNewProject() {
    return {
      name: '',
      fulfilmentTarget: 0,
      description: '',
      dailySkills: [],
      totalSkills: [],
      status: '',
      employees: [],
      manager: {
        userId: '',
        firstName: '',
        lastName: '',
      },
      startDate: '',
      projectId: '',
      checkDate: '',
    };
  }

  private filterMyProjects(): IProject[] {
    return (this.myProjects =
      this.projects &&
      this.projects.filter((project) => {
        return project.manager.userId === this.loggedUser.userId;
      }));
  }
}
