import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProjectEmployeesComponent } from './new-project-employees.component';

describe('NewProjectEmployeesComponent', () => {
  let component: NewProjectEmployeesComponent;
  let fixture: ComponentFixture<NewProjectEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewProjectEmployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProjectEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
