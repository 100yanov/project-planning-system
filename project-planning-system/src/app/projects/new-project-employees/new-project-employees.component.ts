import { IProject } from './../../common/interfaces/project';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from '../../common/interfaces/user';
import { ISkillHoursPerDay } from '../../common/interfaces/skill-hour-per-day';
import { IProjectUsers } from '../../common/interfaces/project-users';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-new-project-employees',
  templateUrl: './new-project-employees.component.html',
  styleUrls: ['./new-project-employees.component.scss'],
})
export class NewProjectEmployeesComponent implements OnInit {
  @Input()
  public employees: IUser[];
  @Input()
  public employeeSkills: string[];
  @Input()
  public newProjectInfo: IProject;
  @Output()
  public addEmployee = new EventEmitter();

  constructor(private readonly notificationService: NotificationService) {}

  ngOnInit(): void {}

  public assignEmployeeSkills(employee: IUser) {
    if (employee) {
      this.employeeSkills = employee.userSkills.filter((s) => s !== 'Admin');
    }
  }

  public ifEmployeeSkillIsAdded(employee: IUser, skill: string): boolean {
    const employeeTocheckSkill = this.newProjectInfo.employees.find(
      (e) => e.userId === employee.userId
    );

    if (employeeTocheckSkill) {
      return employeeTocheckSkill.skillsHoursPerDay.some(
        (sh) => sh.skill === skill
      );
    }
  }

  public addEmployeeToNewProject(
    employee: IUser,
    skill: string,
    hoursPerDay: number
  ) {
    hoursPerDay = +hoursPerDay;

    if (!employee || !skill || !hoursPerDay) {
      this.notificationService.error('Invalid employee info!');
      return;
    }

    if (
      this.newProjectInfo.employees.find((e) => e.userId === employee.userId)
    ) {
      employee.availableTime = this.newProjectInfo.employees.find(
        (e) => e.userId === employee.userId
      ).availableTime;
    }

    if (!this.newProjectInfo.totalSkills.some((ts) => ts.skill === skill)) {
      this.notificationService.error(
        `You cannot add ${skill} since it is not added to this project!`
      );
      return;
    }

    if (isNaN(hoursPerDay) || hoursPerDay < 0) {
      this.notificationService.error(
        'Hours Per Day should be a valid positive number!'
      );
      return;
    }

    const skillAndHours: ISkillHoursPerDay = { skill, hoursPerDay };

    if (this.checkAvailableTime(employee, hoursPerDay)) {
      return;
    }

    let employeeToAddToProject = this.newProjectInfo.employees.find(
      (e) => e.userId === employee.userId
    );

    if (employeeToAddToProject) {
      if (this.checkAvailableTime(employeeToAddToProject, hoursPerDay)) {
        return;
      }

      if (
        this.newProjectInfo.employees.some(
          (e) =>
            e === employeeToAddToProject &&
            e.skillsHoursPerDay.some((sh) => sh.skill === skill)
        )
      ) {
        this.notificationService.error(
          `${employeeToAddToProject.firstName} ${employeeToAddToProject.lastName} with skill: ${skill} has already been added to this project!`
        );
        return;
      }

      this.addEmployee.emit({ employeeToAddToProject, skillAndHours });
    } else {
      employeeToAddToProject = {
        userId: employee.userId,
        firstName: employee.firstName,
        lastName: employee.lastName,
        availableTime: employee.availableTime,
        skillsHoursPerDay: [{ skill, hoursPerDay }],
      };

      if (this.checkAvailableTime(employeeToAddToProject, hoursPerDay)) {
        return;
      }

      this.addEmployee.emit({ employeeToAddToProject, skillAndHours });
    }
  }

  private checkAvailableTime(
    employee: IUser | IProjectUsers,
    hoursPerDay: number
  ) {
    if (hoursPerDay > employee.availableTime) {
      this.notificationService.error(
        `The available time of ${employee.firstName} ${employee.lastName} is ${employee.availableTime} hours!`
      );
      return true;
    }
  }
}
