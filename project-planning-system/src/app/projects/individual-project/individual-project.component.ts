import { ConfirmationDialogComponent } from './../../components/confirmation-dialog/confirmation-dialog.component';
import { ProjectStatus } from './../../common/enums/project-status.enum';
import { IUser } from './../../common/interfaces/user';
import { ISkillHoursPerDay } from '../../common/interfaces/skill-hour-per-day';
import { IProject } from './../../common/interfaces/project';
import { ProjectsService } from './../projects.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProjectUsers } from '../../common/interfaces/project-users';
import { UsersService } from '../../users/users.service';
import { switchMap } from 'rxjs/operators';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { NewProjectComponent } from '../new-project/new-project.component';
import { NotificationService } from '../../core/services/notification.service';
declare const require: any;
const moment = require('moment-business-days');

@Component({
  selector: 'app-individual-project',
  templateUrl: './individual-project.component.html',
  styleUrls: ['./individual-project.component.scss'],
})
export class IndividualProjectComponent implements OnInit {
  public project: IProject;
  public updatedProject: IProject;
  public today: string;
  public projectStartDate: string;
  public passedDaysSinceStarted: number;
  public businessDaysToCompleteProject: string | number;
  public daysUntilCompletion: string | number;
  public isOnTarget: boolean;
  public targetBusinessDays: number;
  public disableButtons: boolean;

  constructor(
    private readonly projectsService: ProjectsService,
    private readonly usersService: UsersService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private dialog: MatDialog,
    private readonly notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap((param) =>
          this.projectsService.getIndividualProject(param.projectId)
        )
      )
      .subscribe(
        async (project) => {
          if (!project) {
            this.router.navigate(['/not-found']);
          }

          this.project = project;

          this.today = moment().format('MM-DD-YYYY');
          this.passedDaysSinceStarted = moment(this.today).businessDiff(
            moment(this.project.startDate)
          );

          this.targetBusinessDays =
            project.fulfilmentTarget - this.passedDaysSinceStarted;

          this.businessDaysToCompleteProject = this.projectsService.checkProjectCompletionDays(
            this.project
          );

          this.daysUntilCompletion = Number(
            this.projectsService.getCompletionDays(this.project)
          );

          this.isOnTarget = this.isProjectOnTarget();

          await this.updateProjectAndEmployees(this.project);
        },
        (err) =>
          this.notificationService.error(
            'Something went wrong, please try again!'
          )
      );
    moment.suppressDeprecationWarnings = true;
  }

  public calculateProjectSkills(project: IProject) {
    const today = moment().format('MM-DD-YYYY');

    const passedDaysSinceStarted = moment(today).businessDiff(
      moment(project.checkDate)
    );

    this.targetBusinessDays = project.fulfilmentTarget - passedDaysSinceStarted;

    if (project.checkDate === today) {
      return project;
    }

    project.dailySkills.forEach((ds) => {
      const totalSkill = project.totalSkills.find(
        (ts) => ts.skill === ds.skill
      );

      totalSkill.hoursLeft -= ds.hoursPerDay * passedDaysSinceStarted;

      if (totalSkill.hoursLeft <= 0) {
        ds.hoursPerDay = 0;
        totalSkill.hoursLeft = 0;

        this.updateProjectEmployees(project, ds);
      }
    });

    const isProjectFinished = project.totalSkills.every(
      (ts) => ts.hoursLeft === 0
    );

    if (isProjectFinished) {
      project.status = ProjectStatus.finished;
    }

    project.checkDate = moment().format('MM-DD-YYYY');
    return project;
  }

  public async calculateEmployeesSkills(
    project: IProject,
    employeesToUpdate: IProjectUsers[]
  ) {
    const employeesFullInfoToUpdate = [];

    await this.asyncForEach(employeesToUpdate, async (e: IProjectUsers) => {
      const employee: IUser = await this.usersService.getIndividualUserPromise(
        e.userId
      );

      if (employee.availableTime === e.availableTime) {
        return;
      }

      const employeeProject = employee.projects.find((p) => {
        return p.projectId === project.projectId;
      });

      if (employeeProject) {
        employeeProject.userSkillsInProject = e.skillsHoursPerDay;
      }

      employee.availableTime = e.availableTime;

      employee.projects = employee.projects.filter((ep) => {
        return !ep.userSkillsInProject.every((usip) => {
          return usip.hoursPerDay === 0;
        });
      });

      employeesFullInfoToUpdate.push(employee);
    });
    return employeesFullInfoToUpdate;
  }

  public async updateProjectAndEmployees(project: IProject) {
    try {
      project = this.calculateProjectSkills(project);

      const employeesToUpdate = project.employees.filter((e) => {
        return e.skillsHoursPerDay.some((shpd) => shpd.hoursPerDay === 0);
      });

      const employeesToUpdateFullInfo = await this.calculateEmployeesSkills(
        project,
        employeesToUpdate
      );

      await this.projectsService.updateProject(project);

      await this.asyncForEach(employeesToUpdateFullInfo, async (e: IUser) => {
        await this.usersService.updateEmployeeAfterProject(e);
      });

      return project;
    } catch (e) {
      this.notificationService.error('Something went wrong, please try again!');
    }
  }

  public onEditProject() {
    this.updatedProject = JSON.parse(JSON.stringify(this.project));

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.disableClose = true;
    dialogConfig.width = '80%';
    dialogConfig.height = '80%';
    dialogConfig.data = {
      project: this.updatedProject,
    };

    const dialogRef = this.dialog.open(NewProjectComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      async (project: IProject) => {
        if (!project) {
          return;
        }

        if (
          !project.name ||
          !project.description ||
          !project.fulfilmentTarget
        ) {
          this.notificationService.error('Invalid project info!');
          return;
        }

        this.project = project;

        project.employees = project.employees.filter(
          (e) => e.skillsHoursPerDay.length !== 0
        );

        this.businessDaysToCompleteProject = this.projectsService.checkProjectCompletionDays(
          this.project
        );

        this.daysUntilCompletion = Number(
          this.projectsService.getCompletionDays(this.project)
        );

        await this.asyncForEach(project.employees, async (e: IProjectUsers) => {
          const employee: IUser = await this.usersService.getIndividualUserPromise(
            e.userId
          );

          employee.projects = employee.projects.filter(
            (p) => p.projectId !== project.projectId
          );

          employee.availableTime = e.availableTime;

          await this.usersService.updateEmployeeAfterProject(employee);
        });

        this.projectsService.addProject(project);
      },
      (err) =>
        this.notificationService.error(
          'Something went wrong, please try again!'
        )
    );
  }

  public onStopProjectClick() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.width = '40%';
    dialogConfig.height = '30%';
    dialogConfig.data = {
      confirmed: true,
    };

    const dialogRef = this.dialog.open(
      ConfirmationDialogComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe((data) => {
      if (data && data.confirmed) {
        this.stopProject(this.project);
      }
    });
  }

  public stopProject(project: IProject) {
    project.totalSkills = project.totalSkills.map((ts) => ({
      skill: ts.skill,
      totalHours: ts.totalHours,
      hoursLeft: 0,
    }));

    project.employees.forEach((e) => {
      e.availableTime += e.skillsHoursPerDay.reduce((acc, shpd) => {
        return acc + shpd.hoursPerDay;
      }, 0);

      e.skillsHoursPerDay = e.skillsHoursPerDay.map((shpd) => ({
        skill: shpd.skill,
        hoursPerDay: 0,
      }));
    });

    project.status = ProjectStatus.finished;

    this.updateProjectAndEmployees(project);
  }

  public async asyncForEach(array: IProjectUsers[], callback) {
    for (let i = 0; i < array.length; i++) {
      await callback(array[i], i, array);
    }
  }

  public disableBtns() {
    return this.project.status === ProjectStatus.inProgress ? false : true;
  }

  public colorIsOnTarget() {
    return this.isOnTarget
      ? { 'background-color': 'rgba(0, 128, 0, 0.658)' }
      : { 'background-color': 'rgba(255, 0, 0, 0.658)' };
  }

  private updateProjectEmployees(
    project: IProject,
    dayliSkill: ISkillHoursPerDay
  ) {
    project.employees.forEach((e) => {
      const hoursToRelease = e.skillsHoursPerDay.find(
        (shpd) => shpd.skill === dayliSkill.skill
      );

      if (hoursToRelease) {
        const skill = e.skillsHoursPerDay.find(
          (shpd) => shpd.skill === dayliSkill.skill
        );
        e.availableTime += skill.hoursPerDay;
        skill.hoursPerDay = 0;
      }
    });
  }

  private isProjectOnTarget() {
    return this.daysUntilCompletion <= this.targetBusinessDays ? true : false;
  }
}
