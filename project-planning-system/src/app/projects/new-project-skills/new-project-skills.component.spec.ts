import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProjectSkillsComponent } from './new-project-skills.component';

describe('NewProjectSkillsComponent', () => {
  let component: NewProjectSkillsComponent;
  let fixture: ComponentFixture<NewProjectSkillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewProjectSkillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProjectSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
