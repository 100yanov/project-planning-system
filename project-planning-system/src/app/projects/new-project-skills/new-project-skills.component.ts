import { ISkillTotalHours } from '../../common/interfaces/skill-total-hours';
import { IProject } from './../../common/interfaces/project';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-new-project-skills',
  templateUrl: './new-project-skills.component.html',
  styleUrls: ['./new-project-skills.component.scss'],
})
export class NewProjectSkillsComponent implements OnInit {
  @Output()
  public projectSkills = new EventEmitter();
  @Input()
  public skills: string[];
  @Input()
  public newProjectInfo: IProject;
  public projectSkillInput: string;
  public totalHoursInput: number;

  constructor(private readonly notificationService: NotificationService) {}

  ngOnInit(): void {}

  public ifSkillAdded(skill: string) {
    return this.newProjectInfo.totalSkills.some((ts) => ts.skill === skill);
  }

  public emitSkills(skill: string, totalHours: number) {
    totalHours = +totalHours;
    const hoursLeft = totalHours;

    if (this.newProjectInfo.totalSkills.some((ts) => ts.skill === skill)) {
      this.notificationService.error(
        `You have already added ${skill} to this project!`
      );
      return;
    }

    if (isNaN(totalHours) || totalHours <= 0) {
      this.notificationService.error('Total Hours should be a positive number');
      return;
    }

    const skills: ISkillTotalHours = { skill, totalHours, hoursLeft };
    this.projectSkills.emit(skills);
  }
}
