import { ProjectStatus } from './../../common/enums/project-status.enum';
import { NotificationService } from './../../core/services/notification.service';
import { ProjectsService } from './../projects.service';
import { IProjectUsers } from '../../common/interfaces/project-users';
import { ISkillTotalHours } from '../../common/interfaces/skill-total-hours';
import { UsersService } from './../../users/users.service';
import { IUser } from './../../common/interfaces/user';
import { SkillsService } from './../../skills/skills.service';
import { Component, OnInit, Inject, Input, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IProject } from '../../common/interfaces/project';
import { AuthService } from '../../auth/auth.service';
import { IAddEmployeeToProject } from '../../common/interfaces/add-employee-to-project';
import { IProjectDescription } from '../../common/interfaces/project-description';
import { Subscription } from 'rxjs';
declare const require: any;
const moment = require('moment-business-days');

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss'],
})
export class NewProjectComponent implements OnInit, OnDestroy {
  @Input()
  public disableCreateProjectBtn = true;
  public showProjectCompletion = false;
  public projectBtnValue: string;
  public skills: string[];
  private loggedUserSubscription: Subscription;
  public loggedUser: IUser;
  public employees: IUser[];
  public employeeSkills: string[];
  public daysToCompleteProject: number | string;
  public daysToCompleteColor: number | string;
  public newProjectInfo: IProject;

  constructor(
    private readonly projectsService: ProjectsService,
    private readonly authService: AuthService,
    private readonly skillsService: SkillsService,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit(): void {
    this.newProjectInfo = this.data.project;

    this.loggedUserSubscription = this.authService.getLoggedUser.subscribe(
      (user) => (this.loggedUser = user)
    );

    this.skillsService.getAllEmployeeSkills().subscribe(
      (skills) => {
        this.skills = skills
          .map((s) => s.skill)
          .filter((s) => {
            return s !== 'Admin';
          });
      },
      (err) =>
        this.notificationService.error(
          'Something went wrong, please try again!'
        )
    );

    this.usersService.getAllUsers().subscribe(
      (users) => {
        this.employees = users.filter((u) => !u.userSkills.includes('Management'));
        const manager = users.find((m) => m.userId === this.loggedUser.userId);
        this.employees = [manager, ...this.employees];
      },
      (err) =>
        this.notificationService.error(
          'Something went wrong, please try again!'
        )
    );

    this.newProjectInfo.checkDate
      ? (this.projectBtnValue = 'Edit')
      : (this.projectBtnValue = 'Create');
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }

  public onAddEmployee(employeeWithSkillAndHours: IAddEmployeeToProject) {
    const employeeToAdd = this.newProjectInfo.employees.find(
      (e) =>
        e.userId === employeeWithSkillAndHours.employeeToAddToProject.userId
    );

    if (employeeToAdd) {
      employeeToAdd.skillsHoursPerDay.push(
        employeeWithSkillAndHours.skillAndHours
      );

      this.updateAvailableTime(employeeWithSkillAndHours, employeeToAdd);
    } else {
      this.newProjectInfo.employees.push(
        employeeWithSkillAndHours.employeeToAddToProject
      );

      this.updateAvailableTime(
        employeeWithSkillAndHours,
        employeeWithSkillAndHours.employeeToAddToProject
      );
    }

    if (employeeWithSkillAndHours.skillAndHours.skill === 'Management') {
      this.newProjectInfo.manager =
        employeeWithSkillAndHours.employeeToAddToProject;

      this.loggedUser.availableTime -=
        employeeWithSkillAndHours.skillAndHours.hoursPerDay;
    }

    this.daysToCompleteProject = this.projectsService.checkProjectCompletionDays(
      this.newProjectInfo
    );
    this.daysToCompleteColor = Number(
      this.projectsService.getCompletionDays(this.newProjectInfo)
    );
    this.ifShowProjectCompletion();
  }

  public onProjectInfo(info: IProjectDescription) {
    this.newProjectInfo.name = info.name;
    this.newProjectInfo.description = info.description;
    this.newProjectInfo.fulfilmentTarget = info.targetDays;
  }

  public removeEmployeeSkillFromProject(employee: IUser, skill: string) {
    const employeeSkillToRemove = this.newProjectInfo.employees.find(
      (e) => e.userId === employee.userId
    );

    const employeeSkillToRemoveIndex = employeeSkillToRemove.skillsHoursPerDay.findIndex(
      (sh) => sh.skill === skill
    );

    const employeeHoursToRelease =
      employeeSkillToRemove.skillsHoursPerDay[employeeSkillToRemoveIndex]
        .hoursPerDay;

    employeeSkillToRemove.availableTime += employeeHoursToRelease;

    employeeSkillToRemove.skillsHoursPerDay.splice(
      employeeSkillToRemoveIndex,
      1
    );

    this.newProjectInfo.dailySkills.find(
      (ds) => ds.skill === skill
    ).hoursPerDay -= employeeHoursToRelease;

    this.daysToCompleteProject = this.projectsService.checkProjectCompletionDays(
      this.newProjectInfo
    );

    this.daysToCompleteColor = Number(
      this.projectsService.getCompletionDays(this.newProjectInfo)
    );
    this.ifShowProjectCompletion();
  }

  public removeSkillFromProject(skillAndTotalHours: ISkillTotalHours) {
    const skillToRemoveIndex = this.newProjectInfo.totalSkills.findIndex(
      (ts) => ts === skillAndTotalHours
    );

    this.newProjectInfo.totalSkills.splice(skillToRemoveIndex, 1);
    this.newProjectInfo.dailySkills.splice(skillToRemoveIndex, 1);

    this.newProjectInfo.employees.forEach((e) => {
      const ifEmployeeHasSkill = e.skillsHoursPerDay.find(
        (shpd) => shpd.skill === skillAndTotalHours.skill
      );

      if (ifEmployeeHasSkill) {
        e.availableTime += ifEmployeeHasSkill.hoursPerDay;

        e.skillsHoursPerDay = e.skillsHoursPerDay.filter(
          (sh) => sh.skill !== skillAndTotalHours.skill
        );
      }
    });

    this.newProjectInfo.employees = this.newProjectInfo.employees.filter(
      (e) => e.skillsHoursPerDay.length !== 0
    );

    this.daysToCompleteProject = this.projectsService.checkProjectCompletionDays(
      this.newProjectInfo
    );
    this.daysToCompleteColor = Number(
      this.projectsService.getCompletionDays(this.newProjectInfo)
    );
    this.ifShowProjectCompletion();
  }

  public OnAddSkillToNewProject(skills: ISkillTotalHours) {
    skills.totalHours = +skills.totalHours;

    this.newProjectInfo.totalSkills.push(skills);
    this.newProjectInfo.dailySkills.push({
      skill: skills.skill,
      hoursPerDay: 0,
    });

    this.daysToCompleteProject = this.projectsService.checkProjectCompletionDays(
      this.newProjectInfo
    );
    this.daysToCompleteColor = Number(
      this.projectsService.getCompletionDays(this.newProjectInfo)
    );
    this.ifShowProjectCompletion();
  }

  public createProject() {
    this.newProjectInfo.status = ProjectStatus.inProgress;
    this.newProjectInfo.startDate = moment().format('MM-DD-YYYY');
    this.newProjectInfo.checkDate = this.newProjectInfo.startDate;
  }

  public colorOfCompletionDays() {
    if (this.newProjectInfo.fulfilmentTarget === 0) {
      return { 'background-color': 'orange' };
    }
    return this.daysToCompleteColor <= this.newProjectInfo.fulfilmentTarget
      ? { 'background-color': 'green' }
      : { 'background-color': 'red' };
  }

  public toggleDisableCreateProjectBtn() {
    if (
      this.newProjectInfo.employees.length === 0 ||
      this.newProjectInfo.fulfilmentTarget === 0
    ) {
      return true;
    }
    return false;
  }

  private ifShowProjectCompletion() {
    this.newProjectInfo.employees.length === 0
      ? (this.showProjectCompletion = false)
      : (this.showProjectCompletion = true);
  }

  private updateAvailableTime(
    employeeWithSkillAndHours: IAddEmployeeToProject,
    employeeToAdd: IProjectUsers
  ) {
    employeeToAdd.availableTime -=
      employeeWithSkillAndHours.skillAndHours.hoursPerDay;

    this.newProjectInfo.dailySkills.find(
      (el) => el.skill === employeeWithSkillAndHours.skillAndHours.skill
    ).hoursPerDay += employeeWithSkillAndHours.skillAndHours.hoursPerDay;
  }
}
