import { ProjectsRoutingModule } from './projects-routing.module';
import { SharedModule } from './../shared/shared.module';
import { AllProjectsComponent } from './all-projects/all-projects.component';
import { NgModule } from '@angular/core';
import { NewProjectComponent } from './new-project/new-project.component';
import { NewProjectDescriptionComponent } from './new-project-description/new-project-description.component';
import { NewProjectSkillsComponent } from './new-project-skills/new-project-skills.component';
import { NewProjectEmployeesComponent } from './new-project-employees/new-project-employees.component';
import { IndividualProjectComponent } from './individual-project/individual-project.component';

@NgModule({
  declarations: [
    AllProjectsComponent,
    NewProjectComponent,
    NewProjectDescriptionComponent,
    NewProjectSkillsComponent,
    NewProjectEmployeesComponent,
    IndividualProjectComponent,
  ],
  imports: [SharedModule, ProjectsRoutingModule],
  exports: [SharedModule],
})
export class ProjectModule {}
