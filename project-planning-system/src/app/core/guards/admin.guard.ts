import { AuthService } from './../../auth/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) { }

  public canActivate(): Observable<boolean> {

    return this.authService.getLoggedUser.pipe(
      map(user => user && user.userSkills.includes('Admin')),
      tap(isAdmin => {
        if (!isAdmin) {

          this.router.navigate(['/dashboard']);
        }
      })
    );
  }
}
