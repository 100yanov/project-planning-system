import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public constructor(
    private readonly angularFireAuth: AngularFireAuth,
    private readonly router: Router
  ) { }

  public canActivate(): Observable<boolean> {

    return this.angularFireAuth.authState.pipe(
      take(1),
      map(authState => !!authState),
      tap(authenticated => {
        if (!authenticated) {

          this.router.navigate(['/login']);
        }
      })
    );
  }
}
